<?php

return [
    'formats' => [
        'zip',
        '7zip',
        'tar.gz',
    ],
];
