<?php

namespace App\Jobs;

use App\Services\FileCompressor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FileCompressionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $folderPath;
    protected $format;

    public function __construct($folderPath, $format)
    {
        $this->folderPath = $folderPath;
        $this->format = $format;
    }

    public function handle(): void
    {
        $compressHandler = new FileCompressor();
        $compressHandler->compress($this->folderPath, $this->format);
    }
}
