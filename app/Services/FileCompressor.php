<?php

namespace App\Services;

use App\Services\Contracts\CompressorInterface;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class FileCompressor
{
    public function compress($folderPath, $format): void
    {
        switch ($format) {
            case 'zip':
                $this->zip($folderPath);
                break;

            case '7zip':
                $this->sevenZip($folderPath);
                break;

            case 'tar.gz':
                $this->tarGz($folderPath);
                break;
        }
    }

    protected function zip($folderPath)
    {
        $zipFileName = $folderPath . '.zip';
        $zip = new ZipArchive;
        $zip->open($zipFileName, ZipArchive::CREATE);

        $files = File::allFiles($folderPath);
        foreach ($files as $file) {
            $filePath = $file->getRealPath();
            $relativePath = 'files/' . basename($filePath);
            $zip->addFile($filePath, $relativePath);
        }

        $zip->close();

        $destinationPath = 'compressions';
        Storage::disk('local')->put($destinationPath . '/' . basename($zipFileName), file_get_contents($zipFileName));

        unlink($zipFileName);
    }

    protected function sevenZip($folderPath)
    {
       //TODO NEXT TIME
    }

    protected function tarGz($folderPath)
    {
        //TODO NEXT TIME
    }
}
