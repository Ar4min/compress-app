<?php

namespace App\Http\Controllers\V1\Compression;

use App\Http\Controllers\Controller;
use App\Jobs\FileCompressionJob;
use App\Services\FileCompressor;
use Illuminate\Http\Request;
use App\Services\CompressHandler;

class FileCompressionController extends Controller
{
    protected FileCompressor $compressHandler;

    public function __construct(FileCompressor $compressHandler)
    {
        $this->compressHandler = $compressHandler;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $folderPath = $request->input('folder_path');
        $formats = config('compressor');

        foreach ($formats as $format) {
            $this->compressAsync($folderPath, $format);
        }

        return response()->json(['message' => 'Compression Is Done'],200);
    }

    protected function compressAsync($folderPath, $format): void
    {
        dispatch(new FileCompressionJob($folderPath, $format));
    }
}
